import { IgniteElement } from "../ignite-html/ignite-element.js";
import { IgniteCallback } from "../ignite-html/ignite-html.js";
import { IgniteTemplate, slot, div, html } from "../ignite-html/ignite-template.js";

/**
 * Makes this IgniteTemplate a tab that will become active or inactive based on the state of the tab.
 * @param {String} name The name of the tab.
 * @param {String} group The tab group to put this tab under, by default this is null.
 * @param {Boolean} active Whether or not this tab is active by default.
 * @returns {IgniteTemplate} Returns this ignite template.
 */
 IgniteTemplate.prototype.tab = function(name, group = null, active = false) {
    //Add the tab class to this element.
    this.class("tab")

    //If active, make this tab active.
    if (active) {
        this.class("active");
        Tabs.change(name, group);
    }

    //What we do is create an IgniteCallback that will get called when
    //the tabchange event is raised. Upon disconnecting the callback we remove
    //the event listener.
    var callback = new IgniteCallback(event => {
        if (this.element) {
            if (event.name == name && event.group == group) {
                if (!this.element.classList.contains("active")) {
                    this.element.classList.add("active");
                }
            } else if (event.group == group) {
                this.element.classList.remove("active");
            }
        }
    }, () => {
        window.removeEventListener("tabchange", callback.callback);
    });

    //Register our callback to the tabchange event.
    window.addEventListener("tabchange", callback.callback);

    //Add this callback to our template so that upon deconstruction our callback is destroyed correctly.
    this._callbacks.push(callback);

    return this;
}

/**
 * Makes this IgniteTemplate a tab button that will become active or inactive based on the current tab and change tabs when clicked.
 * @param {String} name The name of the tab that this button will set to active.
 * @param {String} group The tab group the tab belongs to.
 * @param {Boolean} active Whether or not this button is active, indicating the tab is active.
 * @returns {IgniteTemplate} Returns this ignite template.
 */
IgniteTemplate.prototype.tabButton = function(name, group = null, active = false) {
    //Add the tab-btn class to this element.
    this.class("tab-btn");

    //If active, make the button active.
    if (active) {
        this.class("active");
    }

    //What we do is create an IgniteCallback that will get called when
    //the tabchange event is raised. Upon disconnecting the callback we remove
    //the event listener.
    var callback = new IgniteCallback(event => {
        if (this.element) {
            //See if the group matches.
            if (event.group == group) {
                if (event.name == name) {
                    //Mark this button as active.
                    if (!this.element.classList.contains("active")) {
                        this.element.classList.add("active");
                    }
                } else {
                    //Mark this button as inactive.
                    this.element.classList.remove("active");
                }
            }
        }
    }, () => {
        window.removeEventListener("tabchange", callback.callback);
    });

    //Register our callback to the tabchange event.
    window.addEventListener("tabchange", callback.callback);

    //Add this callback to our template so that upon deconstruction our callback is destroyed correctly.
    this._callbacks.push(callback);

    //Setup our on click to change tabs.
    this.onClick(() => Tabs.change(name, group));

    return this;
}

/**
 * A api class to help easily work with tabs.
 */
class Tabs {
    /**
     * Changes the given tab to active.
     * @param {String} name The name of the tab to change to.
     * @param {String} group The name of the group this tab belongs to if any. Default is null.
     * @param {Boolean} active Whether or not to make this tab active. Default is true.
     */
    static change(name, group = null, active = true) {
        //Init the groups if needed.
        if (Tabs.groups[group] == undefined) {
            Tabs.groups[group] = { };
        }

        //Init the tabs if needed.
        if (group == null && Tabs.tabs[name] == undefined) {
            Tabs.tabs[name] = false;
        }

        //If this tab is supposed to be active make that happen.
        if (active) {
            if (group) {
                //Reset all tabs
                Object.keys(Tabs.groups[group]).forEach(key => Tabs.groups[group][key] = false);

                //Set this tab to active.
                Tabs.groups[group][name] = true;
            } else {
                //Reset all tabs
                Object.keys(Tabs.tabs).forEach(key => Tabs.tabs[key] = false);

                //Set this tab to active.
                Tabs.groups[group][name] = true;
            }
        } else {
            //Set this tab to inactive.
            if (group) {
                Tabs.groups[group][name] = false;
            } else {
                Tabs.tabs[name] = false;
            }
        }

        //Create a tab change event and fire it.
        var changeEvent = new Event("tabchange");
        changeEvent.name = (active ? name : "");
        changeEvent.group = group;
        window.dispatchEvent(changeEvent);
    }

    /**
     * Returns whether or not the given tab is active.
     * @param {String} name The name of the tab.
     * @param {String} group The name of the group this tab belongs to. Default is null.
     */
    static isActive(name, group = null) {
        return ((group && Tabs.groups[group] != undefined && Tabs.groups[group][name]) || (group == null && Tabs.tabs[name] != undefined && Tabs.tabs[name])) ? true : false;
    }
}

Tabs.groups = { };
Tabs.tabs = { };

window.Tabs = Tabs;

export {
    Tabs
}